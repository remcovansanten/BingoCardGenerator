import glob
import os
import time
from random import randint
from PIL import Image


def printAll(strings):
    for name in strings:
        print(name)
    print("----")
    return


def getNewRandomIndex(max, currentIndices):
    if max < len(currentIndices):
        num = 0
    else:
        num = randint(0, max)
        while currentIndices.count(num):
            num = randint(0, max)
    return num


def generateCard(nbSlots, maxIndex):
    currentCard = []
    for i in range(0, nbSlots):
        currentCard.append(getNewRandomIndex(maxIndex, currentCard))
    return currentCard


def generateAllCardIndices(nbSlots, nbImages, nbCards):
    allCards = []
    for i in range(0, nbCards):
        singleCard = generateCard(nbSlots, nbImages - 1)
        while allCards.count(singleCard) >= 1:
            singleCard = generateCard(nbSlots, nbImages - 1)
        allCards.append(singleCard)
    return allCards


def generateCardImage(indices, imagePaths, nbRows, nbCols,
                      skipMiddle, bgPaths, positions):
    sources = []
    maxSize = [0, 0]
    for index in indices:
        src = Image.open(imagePaths[index])
        sources.append(src)
        if (src.size[0] > maxSize[0]):
            maxSize[0] = src.size[0]
        if (src.size[1] > maxSize[1]):
            maxSize[1] = src.size[1]
    if len(bgPaths) > 0:
        image = Image.open(bgPaths[0])
    else:
        image = Image.new("RGB", ((maxSize[0] * nbCols+250),
                          (maxSize[1] * nbRows+250)))
    currentIndex = 0
    for col in range(0, nbCols):
        for row in range(0, nbRows):
            if ((skipMiddle and col == nbCols / 2 and row == nbRows / 2) is False):
                if len(positions) == 0:
                    position = (maxSize[0] * col + maxSize[0] / 2 - sources[currentIndex].size[0] / 2,
                                maxSize[1] * row + maxSize[1] / 2 - sources[currentIndex].size[1] / 2)
                else:
                    position = positions[currentIndex]
                image.paste(sources[currentIndex], position)
                currentIndex = currentIndex + 1
    return image


################################
# Script directory
if __name__ == '__main__':  # Main loop
    filenames = []
    pwd = os.path.dirname(__file__)
    # list of image paths
    srcBackgrounds = pwd + '/images/background'
    srcImages = pwd + '/images/squares'
    outImages = pwd + '/result/' + time.strftime("%Y%m%d-%H%M%S") + '/'
    bgPaths = glob.glob(srcBackgrounds + '/*')
    imagePaths = glob.glob(srcImages + '/*')
    nbImages = len(imagePaths)
    print(nbImages, "images found")
    for fname in imagePaths:
        filenames.append(os.path.basename(fname)[:-4])
    filenames.remove("bonus")
    print(filenames)

    # number of images in each card
    nbSlots = 16
    # number of cards
    nbCards = 15
    # number of rows
    nbRows = 4
    # number of columns
    nbCols = 4
    # Skip middle
    skipMiddle = False

    # Square positions in background
    # 4x4
    squarePositions = [
        (100, 150), (400, 150), (700, 150), (1000, 150),
        (100, 400), (400, 400), (700, 400), (1000, 400),
        (100, 650), (400, 650), (700, 650), (1000, 650),
        (100, 900), (400, 900), (700, 900), (1000, 900),
        ]
    # 4x5
    # squarePositions = [
    #     (125, 250), (325, 250), (525, 250), (725, 250), (925, 250),
    #     (125, 450), (325, 450), (525, 450), (725, 450), (925, 450),
    #     (125, 650), (325, 650), (525, 650), (725, 650), (925, 650),
    #     (125, 850), (325, 850), (525, 850), (725, 850), (925, 850)
    #     ]

    print(f"Generating {nbCards} cards with {nbSlots} indices each and using {nbImages} images")
    allCards = generateAllCardIndices(nbSlots, nbImages, nbCards)

    if (os.path.exists(outImages) is False):
        os.makedirs(outImages)
    cardNum = 0
    for indices in allCards:
        result = generateCardImage(indices, imagePaths, nbRows, nbCols, skipMiddle, bgPaths, squarePositions)
        result.save(outImages + 'bingokaart-{}.jpg'.format(filenames[cardNum]))
        cardNum = cardNum + 1
