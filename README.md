Bingo Card Generator
==================
{Python 3.9}

Python bingo card generator
This script will take the files in the images folder and generate unique Bing Cards

images/background for background images
images/squares for he bingo symbols on the card (in .jpg format)
Calibrated for images of 150x150 and background of 1200x 1050
Prints 4x4 or 4x5 bingocards

Manual:
create folder images/squares
create folder images/background
create folder result

Depending on number of images, print a 4x4 or 4x5 layout lay-out 

Todo:
make it easier/more flexible